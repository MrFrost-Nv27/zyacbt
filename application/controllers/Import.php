<?php

if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}
/**
* ZYA CBT
* Achmad Lutfi
* achmdlutfi@gmail.com
* achmadlutfi.wordpress.com
*/
class Import extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        // $templine = '';

        // // Read in entire file
        // $lines = file('./database.sql');
        // // var_dump($lines);
        // // die;
        // try {
        //     // Loop through each line
        //     foreach ($lines as $line) {
        //         // Skip it if it's a comment
        //         if (substr($line, 0, 2) == '--' || $line == '') {
        //             continue;
        //         }

        //         // Add this line to the current templine we are creating
        //         $templine .= $line;

        //         // If it has a semicolon at the end, it's the end of the query so can process this templine
        //         if (substr(trim($line), -1, 1) == ';') {
        //             // Perform the query
        //             $this->db->query($templine);

        //             // Reset temp variable to empty
        //             $templine = '';
        //         }
        //     }
        // } catch (\Throwable $th) {
        //     echo $th->getMessage();
        // }
        redirect('/', 'refresh');
    }
}